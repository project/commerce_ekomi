<?php

/**
 * Implements hook_autoload_registry_files_alter.
 * @param $files
 * @param $modules
 */
function commerce_ekomi_autoload_registry_files_alter(&$files, $modules) {

  if($modules['commerce_ekomi']->status == 1) {
    $commerce_ekomi_files = array();
    $path = drupal_get_path('module', 'commerce_ekomi');

    //Include all php files inside $path_source. This action is not recursive.
    $path_sources = array(
      '/src/Controller',
    );

    foreach ($path_sources as $path_source) {
      $sources = scandir($path . $path_source);
      foreach ($sources as $source) {
        if(preg_match('/\.php$/', $source)) {
          $commerce_ekomi_files[$path . $path_source . $source] = array(
            'module' => 'commerce_ekomi',
            'weight' => 0,
          );
        }
      }
    }

    $files += $commerce_ekomi_files;
  }
}

/**
 * Implements hook_permissions().
 * @return array
 * @todo Create permission row to make ekomi ratings.
 */
function commerce_ekomi_permission() {
  return array(
    'administer commerce ekomi' => array(
      'title' => t('Administer ekomi'),
      'description' => t('Perform administration tasks for ekomi.'),
    ),
  );
}

/**
 * Implements hook_menu().
 * @return array
 */
function commerce_ekomi_menu() {
  $links = [];
  $links['admin/commerce_ekomi'] = [
    'title' => 'Ekomi configuration',
    'page callback' => ['drupal_get_form'],
    'page arguments' => ['commerce_ekomi_config_form'],
    'access arguments' => ['administer commerce ekomi'],
  ];
  $links['admin/commerce_ekomi/config'] = [
    'title' => 'Ekomi configuration',
    'page callback' => ['drupal_get_form'],
    'page arguments' => ['commerce_ekomi_config_form'],
    'access arguments' => ['administer commerce ekomi'],
    'type' => MENU_DEFAULT_LOCAL_TASK,
  ];
  $links['admin/commerce_ekomi/generate_csv'] = [
    'title' => 'Ekomi Generate csv',
    'page callback' => ['drupal_get_form'],
    'page arguments' => ['commerce_ekomi_download_csv_form'],
    'access arguments' => ['administer commerce ekomi'],
    'type' => MENU_LOCAL_TASK,
  ];
  $links['admin/commerce_ekomi/test'] = [
    'title' => 'Test',
    'page callback' => ['drupal_get_form'],
    'page arguments' => ['commerce_ekomi_test_form'],
    'access arguments' => ['administer commerce ekomi'],
    'type' => MENU_LOCAL_TASK,
  ];
  return $links;
}

/**
 * Configuration form for ekomi.
 */
function commerce_ekomi_config_form($form, &$form_state) {
  $ekomi = new commerceEkomiController();
  return system_settings_form($ekomi->configForm());
}

/**
 * @param $form
 * @param $form_state
 */
function commerce_ekomi_download_csv_form($form, &$form_state) {
  $ekomi = new commerceEkomiDownload();
  return $ekomi->getDownloadForm();
}

/**
 * @param $form
 * @param $form_state
 */
function commerce_ekomi_download_csv_form_submit($form, &$form_state) {
  $ekomi = new commerceEkomiDownload($form_state);
  $ekomi->generateCsv();
}

/**
 * @param $form
 * @param $form_state
 */
function commerce_ekomi_test_form($form, &$form_state) {
  $test = new commerceEkomiTest();
  return $test->getTestForm();
}

/**
 * @param $form
 * @param $form_state
 */
function commerce_ekomi_test_form_submit($form, &$form_state) {
  $test = new commerceEkomiTest();
  $result = $test->testServices($form_state['values']);

  if(isset($result['count'])) {
    drupal_set_message(t('The service seems to work correctly. The account have !num ratings with !avg average',
      array('!num' => $result['count'], '!avg' => $result['avg'])));
  } else {
    drupal_set_message(t('Seems doesn\'t work correctly, something is wrong.'), 'error');
  }
}


/**
 * Implements hook_commerce_order_presave().
 */
function commerce_ekomi_commerce_order_presave($order) {
  //When a order is set as completed from any other status, we try to generate
  //a new order review link
  if($order->status == 'completed' && $order->original->status != 'completed') {
    $ekomi = new commerceEkomiController();
    $ekomi->putOrder($order);
  }
}

/**
 * Implements hook_token_info().
 */
function commerce_ekomi_token_info() {
  $type = array(
    'name' => t('Orders', array(), array('context' => 'a drupal commerce order')),
    'description' => t('Tokens related to individual orders.'),
    'needs-data' => 'commerce-order',
  );

  $order = [];
  $order['order-ekomi-review-link'] = array(
    'name' => t('Ekomi review link', array(), array('context' => 'a drupal commerce order')),
    'description' => t('The Ekomi review link if exists'),
  );

  return array(
    'types' => array('commerce-order' => $type),
    'tokens' => array('commerce-order' => $order),
  );
}

/**
 * Implements hook_tokens().
 * @param $type
 * @param $tokens
 * @param array $data
 * @param array $options
 * @return array
 */
function commerce_ekomi_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  if ($type == 'commerce-order' && !empty($data['commerce-order'])) {
    $order = $data['commerce-order'];
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'order-ekomi-review-link':
          $replacements[$original] = $order->data['ekomi_review_link'];
          break;
      }
    }
  }

  return $replacements;
}

/**
 * @param $order_id
 * @param $mail
 * @param $created
 */
function commerce_ekomi_get_order_row($filename, $order_id, $mail, $created, &$context) {
  $handler = fopen(drupal_realpath($filename), 'a');
  $ekomi = new commerceEkomiController();
  $order = entity_load('commerce_order', array($order_id));
  $link = $ekomi->putOrder($order[$order_id]);
  $data = [
    'order_id' => $order_id,
    'mail' => $mail,
    'created' => $created,
    'link' => $link,
  ];
  fputcsv($handler, $data, ';');
  fclose($handler);

  $context['results'][0] = $filename;
}

/**
 * @param $filename
 */
function commerce_ekomi_process_finished($success, $results, $operations) {
  $link = l(t('Download'), file_create_url($results[0]), ['attributes' => ['target' => '_blank']]);
  drupal_set_message(t('The files has been generated successfully, !download now!', ['!download' => $link]));
}

function commerce_ekomi_theme($existing, $type, $theme, $path) {
  return [
    'commerce_ekomi_widget_block' => [
      'template' => 'commerce_ekomi-widget-block',
      'variables' => [
        'ekomi_language' => '',
        'ekomi_type' => '',
        'ekomi_id' => '',
      ],
    ],
  ];
}

/**
 * Ekomi Widget Block  hooks
 * @todo Implements using a class
 **/

/**
 * Implements hook_block_info().
 * @return mixed
 */
function commerce_ekomi_block_info() {
  $blocks = [
    'commerce_ekomi_widget' => [
      'info' => t('Commerce ekomi widget'),
    ],
  ];
  return $blocks;
}

/**
 * Implements hook_block_configure.
 * @param string $delta
 * @return mixed
 */
function commerce_ekomi_block_configure($delta = '') {

  $form = [];
  if($delta == 'commerce_ekomi_widget') {
    $form['commerce_ekomi_widget_type'] = [
      '#type' => 'radios',
      '#title' => t('Ekomi widget type'),
      '#default_value' => variable_get('commerce_ekomi_widget_type', ''),
      '#required' => TRUE,
      '#options' => [
        'moderator5820978501c13' => t('Classic box 1'),
        'moderator582097c6cf63e' => t('Classic box 2'),
        'moderator582097a3f3113' => t('Classic box 3'),
        'moderator5820982d05efb' => t('Classic wide 1'),
        'moderator5820994212da2' => t('Website label white'),
        'moderator58209a3ac8aa2' => t('Horizontal slider'),
      ],
    ];

    $form['commerce_ekomi_widget_language'] = [
      '#type' => 'select',
      '#title' => t('Language'),
      '#default_value' => variable_get('commerce_ekomi_widget_language', ''),
      '#required' => TRUE,
      '#options' => [
        'de' => t('German'),
        'es' => t('Spanish'),
        'en' => t('English'),
        'fr' => t('French'),
        'it' => t('Italian'),
        'du' => t('Dutch'),
      ],
    ];
  }
  return $form;
}

/**
 * Implements hook_block_save().
 */
function commerce_ekomi_block_save($delta = '', $edit = array()) {
  variable_set('commerce_ekomi_widget_type', $edit['commerce_ekomi_widget_type']);
  variable_set('commerce_ekomi_widget_language', $edit['commerce_ekomi_widget_language']);
}

/**
 * Implement hook_block_view().
 * @param string $delta
 * @return array
 */
function commerce_ekomi_block_view($delta='') {
  $block = [];
  if($delta == 'commerce_ekomi_widget') {
    $ekomi = new commerceEkomiController();
    $vars = [
      'ekomi_type' => variable_get('commerce_ekomi_widget_type'),
      'ekomi_language' => variable_get('commerce_ekomi_widget_language'),
      'ekomi_id' => $ekomi->getInterfaceId(),
    ];
    $block['content'] = theme('commerce_ekomi_widget_block', $vars);
  }
  return $block;
}