<?php

class commerceEkomiController {
  const EKOMI_VERSION = 'cust-1.0.0';
  const EKOMI_API_ENDPOINT = 'http://api.ekomi.de/v2/wsdl';

  private $ekomiInterfaceId;
  private $ekomiInterfacePassword;

  public function __construct() {
    $this->ekomiInterfaceId = variable_get('commerce_ekomi_interface_id', '');
    $this->ekomiInterfacePassword = variable_get('commerce_ekomi_interface_password', '');
    $this->cacheTime = variable_get('commerce_ekomi_cache_time', 3600);
  }

  /**
   * @return array
   */
  public function configForm() {
    $form = [];
    $form['commerce_ekomi_interface_id'] = [
      '#type' => 'textfield',
      '#title' => t('Ekomi id'),
      '#default_value' => $this->ekomiInterfaceId,
    ];
    $form['commerce_ekomi_interface_password'] = [
      '#type' => 'textfield',
      '#title' => t('Ekomi password'),
      '#default_value' => $this->ekomiInterfacePassword,
    ];
    $form['commerce_ekomi_cache_time'] = [
      '#type' => 'textfield',
      '#title' => t('Cache time'),
      '#description' => t('Cache snapshot duration, in segs.'),
      '#default_value' => $this->cacheTime,
    ];

    return $form;
  }

  /**
   * @param $order
   * @return mixed
   */
  public  function putOrder(&$order) {
    $orderId = $order->order_id;

    //We never override the ekomi link. The order only can be rated once.
    if(isset($order->data['ekomi_review_link'])) {
      return $order->data['ekomi_review_link'];
    }

    if ($orderId) {
      $response = $this->call('putOrder', $orderId);
      if ($response && isset($response['done']) && isset($response['link'])) {
        $link =  $response['link'];

        //Save the order link in the database
        $order->data['ekomi_review_link'] = $link;
        return $link;
      }
    }
  }

  public function getSnapshot() {
    $key = 'commerce_ekomi_snapshot';
    if (TRUE || ($cache = cache_get($key)) === FALSE) {
      $rating = array(
        'count' => 0,
        'avg' => 0,
      );
      $api_response = $this->call('getSnapshot');
      if ($api_response && isset($api_response['done']) && $api_response['done'] && isset($api_response['info'])) {
        $rating['count'] = $api_response['info']['fb_count'];
        $rating['avg'] = $api_response['info']['fb_avg'];
      }
      cache_set($key, $rating, 'cache', (time() + $this->cacheTime)); //1-hour cache
      $cache = new stdClass();
      $cache->data = $rating;
    } else {
      $rating = $cache;
    }

    return $rating;
  }

  /**
   * Make the call indicated in the params.
   * @param $method
   * @param array $params
   * @return mixed
   */
  private function call($method, $param1 = '', $param2 = '') {
    $client = new SoapClient(commerceEkomiController::EKOMI_API_ENDPOINT, array('exceptions' => 0));
    $interface = $this->ekomiInterfaceId . '|' . $this->ekomiInterfacePassword;
    $response = $client->$method($interface, commerceEkomiController::EKOMI_VERSION, utf8_encode($param1), utf8_encode($param2));
    $response = @unserialize(utf8_decode($response));
    return $response;
  }
}