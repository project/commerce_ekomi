<?php

/**
 * Page to test the service.
 * Class commerceEkomiTest
 */
class commerceEkomiTest {

  /**
   * @return array
   */
  public function getTestForm() {
    $form = [];
    $form['test'] = [
      '#type' => 'submit',
      '#value' => t('Test web service'),
    ];

    return $form;
  }

  /**
   * @param $values
   */
  public function testServices($values) {
    $ekomi = new commerceEkomiController();
    return $ekomi->getSnapshot();
  }
}