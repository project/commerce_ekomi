<?php

/**
 * Class commerceEkomiDownload
 */
class commerceEkomiDownload {

  private $dateFormat;
  private $dateFromDefault;
  private $dateToDefault;
  private $limit;

  public function __construct($form_state = []) {
    //Today is a default From and To value.
    $this->dateFormat = 'Y-m-d H:i';

    if(isset($form_state['values'])) {
      $this->dateFromDefault = new \DateTime($form_state['values']['from']);
      $this->dateToDefault = new \DateTime($form_state['values']['to']);
      $this->limit = $form_state['values']['limit'];
    } else {
      $this->dateFromDefault = new \DateTime();
      $this->dateToDefault = new \DateTime();
      $this->limit = 100;
    }
  }
  /**
   * @return array
   */
  public function getDownloadForm() {
    $form = [];

    $form['limit'] = [
      '#type' => 'textfield',
      '#title' => t('Num of order to exports'),
      '#default_value' => $this->limit,
    ];
    $form['from'] = [
      '#type' => 'date_select',
      '#date_format' => $this->dateFormat,
      '#title' => t('Date from'),
      '#default_value' => $this->dateFromDefault->format($this->dateFormat),
    ];
    $form['to'] = [
      '#type' => 'date_select',
      '#title' => t('Date to'),
      '#date_format' => $this->dateFormat,
      '#default_value' => $this->dateToDefault->format($this->dateFormat),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Generate'
    ];
    return $form;
  }

  /**
   * Generate CSV with the ekomi links to review from the orders.
   */
  public function generateCsv() {

    $query = db_select('commerce_order', 'o')
      ->fields('o', array('order_id', 'mail', 'created'))
      ->condition('status', 'completed')
      ->condition('created', $this->dateFromDefault->getTimestamp(), '>')
      ->condition('created', $this->dateToDefault->getTimestamp(), '<')
      ->orderBy('order_id', 'asc')
      ->range(0, $this->limit);

    $orders = $query->execute()
      ->fetchAll();

    if(!empty($orders)) {

      //Remove the old file if exist.
      $filename = 'public://ekomi_orders.csv';
      unlink(drupal_realpath($filename));

      $batch = array(
        'title' => t('Exporting data ...'),
        'finished' => 'commerce_ekomi_process_finished',
      );

      $operations = [];
      foreach ($orders as $order) {
        $operations[] = array('commerce_ekomi_get_order_row', array($filename, $order->order_id, $order->mail, $order->created));
      }

      $batch['operations'] = $operations;
      batch_set($batch);
      batch_process();
    }
  }

}