<div id="widget-container" class="ekomi-widget-container ekomi-widget-<?php print $ekomi_type ?>"></div>
<script type="text/javascript">

  (function(w) {
    w['_ekomiServerUrl'] = (document.location.protocol=='https:'?'https:':'http:') + '//widgets.ekomi.com';
    w['_customerId'] = <?php print $ekomi_id ?>;
    w['_ekomiDraftMode'] = true;
    w['_language']="<?php print $ekomi_language ?>";

    if(typeof(w['_ekomiWidgetTokens']) !== 'undefined'){
      w['_ekomiWidgetTokens'][w['_ekomiWidgetTokens'].length] = '<?php print $ekomi_type ?>';
    } else {
      w['_ekomiWidgetTokens'] = new Array('<?php print $ekomi_type ?>');
    }
    if(typeof(ekomiWidgetJs) == 'undefined') {
      var s = document.createElement('script');
      s.src = w['_ekomiServerUrl']+'/js/widget.js';
      s.async = true;
      var e = document.getElementsByTagName('script')[0];
      e.parentNode.insertBefore(s, e);
      ekomiWidgetJs = true;
    }

  })(window);

</script>